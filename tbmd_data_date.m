
%%Return initial tensor [it also considers date]
%label_user: label for users
function [D, num_user, num_prod, label_user] = tbmd_data_date()

addpath(genpath('./featureExtractor'));

%load('/Users/anahita/tensor_osd/Data/data_half2013.mat');
    
%user_id = C_new{1};
%prod_id = C_new{2};
%rating = str2double(C_new{3});
%label = str2double(C_new{4});
%date = cellstr(datestr(datevec(C_new{5}),'dd-mmm-yyyy'));

%%user_id = zeros(4, 1);
%%user_id(1) = 1;
%%user_id(2) = 2;
%%user_id(3) = 3;
%%user_id(4) = 4;

%%prod_id = zeros(4, 1);
%%prod_id(1) = 1;
%%prod_id(2) = 1;
%%prod_id(3) = 1;
%%prod_id(4) = 2;

%%rating = zeros(4, 1);
%%rating(1) = 5;
%%rating(2) = 1;
%%rating(3) = 1;
%%rating(4) = 5;
load('/Users/anahita/priors/data/synthetic-yelp-gar-20020012-date2-slowly.mat');

%%label = zeros(4, 1);
%%label(1) = -1;
%%label(2) = 1;
%%label(3) = 1;
%%label(4) = 1;
%12
%load('/Users/anahita/priors/data/synthetic-yelp-gar-20020012-date2-slowly.mat');

%load('/Users/anahita/priors/data/yelp-12013.mat');
%load('/Users/anahita/priors/data/synthetic-ga.mat');
%load('/Users/anahita/priors/data/synthetic-ga.mat');
%load('/Users/anahita/priors/data/synthetic-yelp-gar.mat');
%load('/Users/anahita/priors/data/synthetic-yelp-gar-20020004-date2-slowly.mat');

%%load('/Users/anahita/priors/data/synthetic-fraudeagle.mat');
%%%%load('/Users/anahita/priors/data/synthetic-yelp-gar-20020012-date2.mat');
%load('/Users/anahita/priors/data/synthetic-fraudeagleO-date.mat');
%MN - March 17, 2018
%%%%load('/Users/anahita/priors/data/synthetic-fraudeagleO-date-slowly.mat');
%%%%load('/Users/anahita/priors/data/synthetic-yelp-gar-20020012-date2-slowly.mat');
%%%%load('/Users/anahita/priors/data/synthetic-fraudeagle-gar-20020012-date2-slowly.mat');
%%load('/Users/anahita/priors/data/synthetic-yelp-gar-20020012-date2-slowly-diff.mat');
%1
%load('/Users/anahita/priors/data/synthetic-yelp-gar-200200010-date2-slowly1.mat');
%2
%load('/Users/anahita/priors/data/synthetic-yelp-gar-200200006-date2-slowly.mat');
%syntheic
%%load('/Users/anahita/priors/data/synthetic-fraudeagleO-date-slowly.mat');
%load('/Users/anahita/priors/data/synthetic-fraudeagleO-date-slowly.mat');
%load('/Users/anahita/priors/data/yelp_12013_date.mat');

user_id = C_new{1};
prod_id = C_new{2};
%%syntheticuser_id = str2double(C_new{1});
%%syntheticprod_id = str2double(C_new{2});
%fix it
%%syntheticrating = str2double(C_new{3});
rating = C_new{3};
label_user = C_new{6};%label for users
label = C_new{4};
date = cellstr(datestr(datevec(C_new{5}),'dd-mmm-yyyy'));
    
num_user = size(unique(user_id), 1);
num_prod = size(unique(prod_id), 1);
num_date = size(unique(date), 1);

D = zeros(num_user, num_prod, num_date);

[~,userID] = ismember(user_id,unique(user_id));
[~,prodID] = ismember(prod_id,unique(prod_id));
[~,dateID] = ismember(date, unique(date));
%disp(dateID(1:10));

for m=1:num_date
    idx_m = dateID == m;
    
    users_m = userID(idx_m);
    prods_m = prodID(idx_m);
    rates_m = rating(idx_m);
    dates_m = dateID(idx_m);
    
    %D(users_m, prods_m, m) = rates_m;
    %disp(size(D(users_m, prods_m, m)));
    
    for i=1:size(users_m, 1)
        D(users_m(i), prods_m(i), m) = rates_m(i);
    end
    
    %disp('haha\n');
    %D(users_m, prods_m, m) = rates_m;
end

%%1: relation1: adjacency matrix
%%%reladj = zeros(num_user+num_prod, num_user+num_prod);
%%%fprintf('creating relation 1: having reviewed\n');
%%%for i=1:size(label, 1)
%%%    reladj(userID(i), num_user + prodID(i)) = 1;
%%%end

%%2: relation2: rating matrix
%relrat = zeros(num_user+num_prod, num_user+num_prod);
%fprintf('creating relation 2: rating\n');
%for i=1:size(label, 1)
%    relrat(userID(i), num_user + prodID(i)) = rating(i);
%end

%%3 relation3: average rating deviation
%%%relavgRD = zeros(num_user+num_prod, num_user+num_prod);
%%%fprintf('creating relations 3: rating dev\n');
%%%[user_RD,prod_RD] = avgRD(userID, prodID, rating);
%%%for i = 1:num_user
%%%    for j = 1:num_user
%%%        relavgRD(userID(i), userID(j)) = user_RD(i) - user_RD(j);
%%%    end
%%%end
%%%for i = 1:num_prod
%%%    for j = 1:num_prod
%%%        relavgRD(num_user+prodID(i), num_user+prodID(j)) = prod_RD(i) - prod_RD(j);
%%%    end
%%%end

%fprintf('stacking on tensor\n');
%D(:, :, 1) = relrat;
%%%D(:, :, 2) = relavgRD;
%fprintf('done\n');





%write into the file for hierarchical community structure
%%fileID = fopen('gen-louvain/graph.txt','w');
%%fprintf(fileID,'%d %d\n', [user_id prod_id]');
%%fclose(fileID);
    