# README #

This repository is provided to reproduce the results of the proposed method "SETF: SEmi-supervised Tensor-based Fraud Detection".


### How do I get set up? ###

SETF has been implemented in matlab.

Startup your MATLAB, and simply run

main

NOTE: labeled users and dataset must be given as input to the methods in main.m

To dive into the implementation of SETF, please look at the following files:

SETF.m

tbmd_data_date.m
