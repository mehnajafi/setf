%%Author: Mehrnaz Najafi
%%Implementation of SETF: Semi-supervised tensor-based fraud detection
function [X, W] = SETF(T, L, rank, num_user, num_prod, alpha, learning_rate, beta)

addpath(genpath('./CrossAssociations'));

rng('default')
printitn = 1;
num_rel  = size(T, 3);
num_labels = 2;

%%Initialization
%%%%building tensor 
fprintf('Building tensor\n');
%B = randn(num_user+num_prod,rank);
%S = randn(num_rel,rank);
%A = {B,B,S};
%TT = ktensor(A);
TT = tensor(T);
normT = norm(TT);



%%%%initialization of weight matrix
fprintf('Initializing weight matrix\n');
W = randn(rank, num_labels);

%%%%initialization of factor matrices
fprintf('Initializing latent factor matrices \n');
M = randn(num_user, rank);%user and product latent factor (first dimension latent factor)
M = orth(M);
R = randn(num_rel, rank);%third dimension latent factor
P = randn(num_prod, rank);%second dimension latent factor


%%Classification loss term related
U = zeros(num_user, num_labels);
for i=1:num_user
    if(L(i, 1) ~= 0 || L(i, 2) ~=0)
        U(i, 1) = L(i, 1);
        U(i, 2) = L(i, 2);
    else
        %spammer (pesimistic view!) changed view - Jan 2 2018
        U(i, 1) = 0;
        U(i, 2) = 1;
    end
end

%%Iteration related parameters
opts.record = 0;
opts.mxitr  = 20;
opts.xtol = 1e-5;
opts.gtol = 1e-5;
opts.ftol = 1e-8;
maxIter = 200;
fitchangetol = 1e-4;
fit = 0;

for iter = 1:maxIter
    fitold = fit;
    
    fprintf('Decomposing tensor using ADMM\n');
    
    %%computing second latent factor P (product latent factor)
    %%tensor matricization (mode -2 )
    T2 = tens2mat(T, 2);
    fprintf('computing P\n');
    right_side = -2*T2*kr(R,M);
    %kr(R,M) * kr(R,M)'
    left_side1 = (R'*R).*(M'*M);
    left_side  = - 2*left_side1;
    %%- mu*eye(size(left_side1, 1), size(left_side1, 2));
    P = left_side\right_side';
    P = P';

    %%computing third latent factor R (time latent factor)   
    %%tensor matricization (mode -3)
    fprintf('computing R\n');
    T3 = tens2mat(T, 3);
    %kr(P,M) * kr(P,M)'
    ete = (P' * P) .* (M' * M);
    R = (ete\(transpose(kr(P,M))*T3'))';
    
    
    %computing groups
    fprintf('finding induced subgraph\n');
    %%t = 1;
    findInducedGraph(M, W, '/Users/anahita/priors/data/synthetic-yelp-gar-20020012-date2-slowly.mat', 600);
    s = dlmread('/Users/anahita/priors/data/GroupYelp100Date/induced_graph.txt');
    s(:,3)=1; A=spconvert(s);
    isSelfGraph = false;
    [k,l,Nx,Ny,Qx,Qy,Dnz] = cc_search(A,'hellscream',isSelfGraph);
    groups = comparison(Qx, num_user);
    
    %%computing first latent factor M (user factor)
    %%tensor matricization (mode -1 )
    fprintf('computing M\n');
    %%[M, ~] = OptStiefelGBB(M, @Mfun, opts, alpha, R, P, T, c, U, W, Y, mu, num_user);
    
    learning_rate_m = learning_rate;
    T1 = tens2mat(T, 1);
    %kr(R,P) * kr(R,P)'
    ete = (R' * R) .* (P' * P);
    general_gradient_m = -2 * (T1 * kr(R, P) - M * ete);
    
    %%Feb 18 2018 - labeled data
    idxs = L(:, 1) == 1 | L(:, 2) == 1;
    M_labeled = M(idxs, :);
    U_labeled = U(idxs, :);
    gradient_m_labeled = general_gradient_m(idxs, :) + 2 * alpha * (M_labeled * W - U_labeled) * W';
    M_int_labeled = M_labeled - learning_rate_m * gradient_m_labeled;
    M(idxs, :) = M_int_labeled;
    
    %%Feb 18 2018 - unlabeled data
    idxs = L(:, 1) == 0 & L(:, 2) == 0;
    M_unlabeled = M(idxs, :);
    gradient_m_unlabeled = general_gradient_m(idxs, :);
    M_int_unlabeled = M_unlabeled - learning_rate_m * gradient_m_unlabeled;
    M(idxs, :) = M_int_unlabeled;
    
    [M_int, ~] = prox_gl(M, beta, groups);
    [M, ~] = prox_gle(M_int, beta, groups);

    
    %%computing W
    fprintf('computing W\n');
    idxs = L(:, 1) == 1 | L(:, 2) == 1;
    M_labeled = M(idxs, :);
    U_labeled = U(idxs, :);
    ete =  M_labeled' * M_labeled;
    %%lambda1 = 0.0001;
    %%left_side = ete + 2 * lambda1 * eye(size(ete, 1), size(ete, 2));
    left_side = ete;
    right_side = M_labeled' * U_labeled;
    epsilon = 0.001;
    W = (left_side + epsilon * eye(size(left_side, 1), size(left_side, 2))) \ right_side;
    %%W = left_side \ right_side;
    
    %%checking the objective function value
    
    %%if iter>2
    %%    Obj_diff = (obj(iter-1)-obj(iter)) /obj(iter-1);
    %%    
    %%    if Obj_diff < thresh
    %%        break;
    %%    end
    %%end
    %%disp(norm(M(1:num_user,:) * W - c .* U, 'fro')^2);
    %%disp(norm(W, 2)^2);
    
    X = ktensor({M,P,R});
    tempX = tensor(X);
    
    normresidual = sqrt(normT^2 + norm(X)^2 - 2 * innerprod(TT,tempX));
    fit = 1 - (normresidual / normT);
    fitchange = abs(fitold - fit);
    
    if mod(iter,printitn)==0
        fprintf(' Iter %2d: fitdelta = %7.1e\n', iter, fitchange);
    end
    
    % check for convergence
    if (iter > 1) && (fitchange < fitchangetol)
        break;
    end
    
end

X = arrange(X); % columns are normalized

end


%%group regularizer
function [M_final, val] = prox_gl(M, beta, groups)
    num_clusters = size(unique(groups), 1);
    M_final = M;
    
    for c=1:num_clusters
        members = groups == c;
        norm_2 = norm(M_final(members, :), 2);
        num = max(0, 1 - (beta/norm_2));
        
        if(num > 0)
            result = num .* (M_final(members, :));
            M_final(members, :) = result;
        else
            M_final(members, :) = 0.0001 .* ones(size(M_final(members, :)));
        end
    end
    
    val = 0;
    for c=1:num_clusters
        members = groups == c;
        val = val + norm(M_final(members, :), 2);
    end
end

%%exclusive group sparsity
function [M_final, val] = prox_gle(M, beta, groups)
    num_clusters = size(unique(groups), 1);
    M_final = M;
    
    for c=1:num_clusters
        members = groups == c;
        norm_1 = norm(M_final(members, :), 1);
        for i=1:size(M_final, 1)
            if(members(i) == 1)
                for j=1:size(M_final, 2)
                    num = max(0, abs(M_final(i,j)) - beta * norm_1 );
                    if(num > 0)
                        result = num * sign(M_final(i,j));
                        M_final(i, j) = result;
                    else
                        M_final(i, j) = 0.0001 * sign(M_final(i,j));
                    end
                end
            end
        end
    end
    
    val = 0;
    for c=1:num_clusters
        members = groups == c;
        val = val + norm(M_final(members, :), 2);
    end
end