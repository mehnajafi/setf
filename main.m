%Author: Mehrnaz Najafi
%runs SETF: semi-supervised tensor-based fraud detection

addpath(genpath('./tensorlab'));
addpath(genpath('./tensor_toolbox_2.6'));
%addpath(genpath('./FOptM'));


%loads data into tensor
[D, num_user, num_prod, label] = tbmd_data_date();


%%Ground Truth for user
GT = zeros(size(label, 1), 2);
%1 good -1 bad
%gt
for i=1:size(label, 1)
    if(label(i, 1) == 1)
        %1 benign 2 spammer
        GT(i, 1) = 1;
        GT(i, 2) = 0;
    else
        GT(i, 1) = 0;
        GT(i, 2) = 1;
    end
end
[~, gtc] = max(GT, [], 2);


for t=1:5
    num_labeled = 440;
    
    %%loads labeled users
    fileID = fopen(strcat('/Users/anahita/priors/data/GroupYelp100Date/Nlabeledusersslowly', num2str(t), '.txt'),'r');
    formatSpec = '%d';
    A = fscanf(fileID,formatSpec);
    
    %%train dataset
    Y = zeros(size(label, 1), 2);
    %1 benign 2 spammer
    %zero: unknown because we are making it semi-supervised
    for i=1:size(label, 1)
        if(A(i) ~= 0 && GT(i, 1) == 1)
            Y(i, 1) = 1;
            Y(i, 2) = 0;
        end
        if(A(i) ~= 0 && GT(i, 2) == 1)
            Y(i, 1) = 0;
            Y(i, 2) = 1;
        end
    end

%hyperparameters
alpha = 100;
learning_rate = 0.001;
beta = 0.1;
rank = 7;

[X, W] = SETF(D, Y, rank, num_user, num_prod, alpha, learning_rate, beta);

M = X{1};
[~, pdc] = max(M * W, [], 2);

%stores predicitons
fileID = fopen(strcat('/Users/anahita/priors/data/GroupYelp100Date/predicted012', num2str(t), '.txt'),'w');
fprintf(fileID,'%d\n', pdc);
fclose(fileID);

%%stores labeled users
fileID = fopen('/Users/anahita/priors/data/GroupYelp100Date/gtSETSSlowly006.txt', 'w');
fprintf(fileID,'%d\n', gtc);
fclose(fileID);

end

