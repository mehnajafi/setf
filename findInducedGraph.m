%%Author: Mehrnaz Najafi
%%Implementation of finding induced graph of top users
function [] = findInducedGraph(M, W, datafile, top_users)


scores = M * W;
for i=1:size(scores, 1)
    result = 1 / (1 + exp(-scores(i)));
    scores(i) = result;
end

%%find induced subgraph of users and the products they have reviewed
[~,idx] = sort(scores, 'descend');
%sorted_scores = scores(idx,:);

%%load data file
load(datafile);
user_id = C_new{1};
prod_id = C_new{2};
%%rating = C_new{3};
%%label = C_new{4};
%%date = cellstr(datestr(datevec(C_new{5}),'dd-mmm-yyyy'));
          
% adjacency list of the review network
[~,userID] = ismember(user_id,unique(user_id));
[~,prodID] = ismember(prod_id,unique(prod_id));

unique_induced_users = idx(1:top_users);
induced_user_id = 0.5 .* ones(size(userID, 1), 1);
induced_prod_id = 0.5 .* ones(size(prodID, 1), 1);

%this file will be later used by comparison.m
fileID = fopen('/Users/anahita/priors/data/GroupYelp100Date/inducedusers.txt','w');
fprintf(fileID,'%d\n', unique_induced_users);
fclose(fileID);

count = 1;
for i=1:size(userID, 1)
    user = userID(i);
    if(ismember(user, unique_induced_users))
        index = find(unique_induced_users == user);
        induced_user_id(count) = index;
        induced_prod_id(count) = prodID(i);
        count = count + 1;
    end
end
count = count - 1;


graph = [induced_user_id(1:count), induced_prod_id(1:count)];
fileID = fopen('/Users/anahita/priors/data/GroupYelp100Date/inducedgraph.txt','w');
fprintf(fileID,'%d %d\n', graph);
fclose(fileID);

% adjacency list of the review network
users = induced_user_id(1:count);
prods = induced_prod_id(1:count);

%[~,induceduserID] = ismember(users,unique(users));
[~,inducedprodID] = ismember(prods,unique(prods));


graphI = zeros(size(users, 1), 2);
graphI(:, 1) = users;
graphI(:, 2) = inducedprodID;
fileID = fopen('/Users/anahita/priors/data/GroupYelp100Date/induced_graph.txt','w');

for i=1:size(users, 1)
    fprintf(fileID,'%d ', graphI(i, 1));
    fprintf(fileID,'%d', graphI(i, 2));
    fprintf(fileID, '\n');
end
fclose(fileID);
